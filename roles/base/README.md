# base role

A base role for common packages and configuration.

## Variables

var | default | description
--- | ------- | -----------
inventory_hostname | | Domain (fetched from inventory)
ansible_port | | ssh port (fetched from inventory)
