# mail role

A mail role for basic Postfix configuration for outgoing mail and systmed notifications.

## Variables

var | default | description
--- | ------- | -----------
inventory_hostname | | Domain (fetched from inventory)
team_email | | Email recipient address for alerts
