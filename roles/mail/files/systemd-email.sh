#!/bin/bash

echo "$(systemctl status -l -n 100 "$2")" | /usr/bin/mail -s "[libreops] $2" "$1"
