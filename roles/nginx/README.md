# nginx role

A nginx role for default web server configuration.

## Variables

var | default | description
--- | ------- | -----------
inventory_hostname | | Domain (fetched from inventory)
ansible_port | | ssh port (fetched from inventory)
data_dir | /data/ | Encrypted data folder
noreply_email | | Outgoing emails sender address
