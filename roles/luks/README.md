# luks role

A luks role for the init script for unlocking the encrypted disk.

## Variables

var | default | description
--- | ------- | -----------
root_bin_dir | /root/bin/ | path for root only scipts
luks_init_cmd | | The decryption command string
