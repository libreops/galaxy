# swap role

A swap role for creating a swapfile.

## Variables

var | default | description
--- | ------- | -----------
swap_size | 20480 | Swap file size (fetched from inventory)
