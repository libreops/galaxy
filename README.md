# Ansible Collection - librops.cloud

<a href="https://libreops.cc"><img src="https://libreops.cc/static/img/libreops.svg" width="300"></a>

LibreOps anslible collection of commonly used roles.

## Usage

1. You need to install the collection first in order to use the roles included

```bash
ansible-galaxy collection install git+https://gitlab.com/libreops/galaxy
```

2. Then you can include any role you want using its fully qualified collection name (FQCN).

This can be done in a playbook:

```yml
roles:
  - libreops.cloud.nginx
```

or as a task in another role

```bash
- name: Import a role
  import_role:
    name: libreops.cloud.nginx

```

### Variables

For every role used you should also check if the variables described at its README file have been set.

## Support

[![OpenCollective](https://img.shields.io/opencollective/all/libreops?color=%23800&label=opencollective&style=flat-square)](https://opencollective.com/libreops/)

## Join

[![irc](https://img.shields.io/badge/Matrix-%23libreops:matrix.org-blue.svg)](https://riot.im/app/#/room/#libreops:matrix.org)

## License

[![license](https://img.shields.io/badge/license-AGPL%203.0-6672D8.svg)](LICENSE)
